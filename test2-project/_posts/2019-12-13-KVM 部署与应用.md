---
layout: post
title:  "KVM 部署与应用"
categories: Linux
tags:  云计算
author: 飞仔
---

* content
{:toc}


## 知识点

1、RAW 采用预分配空间方式 , 也就是虚拟机磁盘 20G , 物理主机就会产生一个20G的文件
2、QCOW2 采用延迟分配方式 , 一开始磁盘文件在物理主机上只占很小的空间 , 随着向虚拟机写入数据 , 这个硬盘文件会自动增长

默认位置：/var/lib/libvirt/images





## KVM部署

    首先验证CPU是否支持虚拟化，输入有vmx或svm就支持，支持虚拟化则就支持KVM
    cat /proc/cpuinfo | egrep 'vmx|svm'
    
    查看是否加载KVM
	
    lsmod | grep kvm
    
    没有加载，执行以下命令加载KVM
	
    modprobe kvm
    
    安装KVM相关软件包
	
    yum install qemu-kvm qemu-img virt-manager libvirt libvirt-python virt-manager  libvirt-client virt-install virt-viewer -y
    
    启动libvirt并设置开机自启动
	
    systemctl start libvirtd
    systemctl enable libvirtd
    
    命令行安装QCOW2  
	
    screen -S kvm-ldap
    virt-install --name ldap --ram=2048 --vcpus=1 --location=/home/ISO/CentOS-7.3-x86_64-DVD-1611.iso --disk path=/kvm/ldap/ldap.qcow2,size=50 --network bridge=kvm --graphics none --console pty,target_type=serial   --extra-args 'console=ttyS0,115200n8 serial'  --force
    

    暂时离开会话：Ctrl+a  d

    查看会话列表: screen -ls

    恢复之前离开的会话：screen -r 会话名或进程号

    清除dead状态的会话：screen -wipe

##  配置网络环境

搭建桥接环境

    yum -y install bridge-utils 

宿主机的网络配置


    cat /etc/sysconfig/network-scripts/ifcfg-em1

    DEVICE="em1"
    
    ONBOOT=yes
    
    BRIDGE=kvm

##  对桥接设备的修改
	
	
```
## 对桥接设备的修改

 vi /etc/sysconfig/network-scripts/ifcfg-kvm
    
    DEVICE="kvm"TYPE=Bridge
    
    BOOTPROTO=static
    
    ONBOOT=yes
    
    IPADDR=192.168.5.254
    
    NETMASK=255.255.255.0
    
    GATEWAY=192.168.5.1
    
    DNS1=223.5.5.5
    
    DNS2=180.76.76.76
```

##  virsh 相关命令
    查看帮助
    virsh --help
    
    列出运行状态的虚拟机
    virsh list
    
    列出所有的虚拟机
    virsh list --all
    
    列出虚拟网络
    virsh net-list
    
    查看指定虚拟机的信息
    virsh dominfo 虚拟机名称
    
    启动虚拟机
    virsh start 虚拟机名称
    
    重启虚拟机
    virsh reboot 虚拟机名称
    
    关闭虚拟机
    virsh shutdown 虚拟机名称
    
    强制关闭虚拟机
    virsh destroy 虚拟机名称
    
    设置虚拟机开机自启
    virsh autostart 虚拟机名称
    
    创建虚拟机快照
    virsh snapshot-create-as --domain 虚拟机名称 --name 快照名 --description "快照备注"
    
    还原虚拟机快照
    virsh snapshot-revert 虚拟机名称 快照名
    
    删除虚拟机快照
    virsh snapshot-delete --domain 虚拟机名称 --snapshotname 快照名
    
    查询虚拟机快照详情
    virsh snapshot-list --domain 虚拟机名称
    
    删除虚拟机定义
    virsh undefine 虚拟机名称













