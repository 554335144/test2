---
layout: page
title: About
permalink: /about/
icon: heart
type: page
---

* content
{:toc}

## 关于我

一直以来自己有一个不知道大家有没有的困惑，那就是遗忘这个东西实在太严重了！学过的东西，实践过的知识，甚至自己曾经轻车熟路的，随着时间的流逝，都慢慢的在记忆中模糊了，变淡了，甚至遗忘了。很讨厌这种感觉，在如今95后大牛们都 “大行其道” 的今天，随着自己年龄的增长，我不想让我的所学所感从我手中轻易流逝，我想抓住他，我想将它留在某个地方，我想将它镌刻在某个地方…… 无疑，自己的技术博客是最好的场所。


然而重度拖延症的我一直带着这个想法很久也没有付诸行动，直到今年，我才将这个事情提上日程，让自己多学习，多记录，多分享，所以就这样开始了旅途……


## 论文

[中国计算机学会推荐国际学术会议和期刊目录](https://www.ccf.org.cn/xspj/gyml) \|
[中国科技论文在线](http://www.paper.edu.cn)

## 镜像资源

[中国科学技术大学](http://mirrors.ustc.edu.cn) \|
[清华大学](https://mirrors.tuna.tsinghua.edu.cn) \|
[阿里巴巴](https://opsx.alibaba.com/?lang=zh-CN/)

## Google 工具

[Google Search](https://www.google.com) \| 
[Google Translate](https://translate.google.com/) \| 
[Gmail](https://mail.google.com/)

## 友链
[ Y。S。H。](https://me.csdn.net/yangshihuz)

## Comments

{% include comments.html %}
